@extends('app.layouts.basico')
@section('titulo', 'Fornecedor')

@section('conteudo')


    <div class="conteudo-pagina">
        <div class="titulo-pagina-2">
            <p>fornecedor</p>
        </div>

        <div class="menu">
            <li><a href="{{ route('app.fornecedor.adicionar') }}">Novo</a></li>
            <li><a href="{{ route('app.fornecedor') }}">Consulta</a></li>

        </div>

        <div class="informacao-pagina">
            <div style="width: 30%; margin-left: auto; margin-right: auto;">
                <form method="post" action="{{ route('app.fornecedor.listar') }}">
                    @csrf
                    <input type="text" class="borda-preta" name="nome" placeholder="Digite o nome">
                    <input type="text" class="borda-preta" name="site" placeholder="Digite o site">
                    <input type="text" class="borda-preta" name="uf" placeholder="Digite o uf">
                    <input type="email" class="borda-preta" name="email" placeholder="Digite o email">
                    <button type="submit" class="borda-preta">Pesquisar</button>
                </form>
            </div>
        </div>

    </div>
@endsection
