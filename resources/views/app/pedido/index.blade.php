@extends('app.layouts.basico')
@section('titulo', 'Produto')

@section('conteudo')


    <div class="conteudo-pagina">
        <div class="titulo-pagina-2">
            <p>Listagem Pedidos</p>
        </div>
        <div class="menu">
            <li><a href="{{ route('pedido.create') }}">Cadastrar Novo Pedido</a></li>
        </div>
        <div class="informacao-pagina">
            <div style="width: 90%; margin-left: auto; margin-right: auto;">
                <table border="1" width="100%">
                    <thead>
                        <tr>
                            <th>Id Pedido</th>
                            <th>Cliente</th>
                            <th></th>
                            <th>Ações</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pedidos as $pedido)
                            <tr>
                                <td>{{ $pedido->id }}</td>

                                <td>{{ $pedido->cliente_id }}</td>
                                <td> <a href="{{route('pedido-produto.create',['pedido' => $pedido->id])  }}">Adicionar Produtos</a> </td>
                                <td>
                                    <a href="{{ route('pedido.show', ['pedido' => $pedido->id]) }}">Visualizar</a>|
                                    <a href="{{ route('pedido.edit', ['pedido' => $pedido->id]) }}">Editar</a>|
                                    <form id="form_{{ $pedido->id }}" method="post"
                                        action="{{ route('pedido.destroy', ['pedido' => $pedido->id]) }}">
                                        @method('DELETE')
                                        @csrf
                                        <a href="#"
                                            onclick="document.getElementById('form_{{ $pedido->id }}').submit()">Excluir</a>
                                    </form>
                                <td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $pedidos->appends($request)->links() }}
                <br>
                Exbindo {{ $pedidos->count() }} Produtos de {{ $pedidos->total() }} de(
                {{ $pedidos->firstItem() }}
                a {{ $pedidos->lastItem() }})
            </div>
        </div>
    </div>
@endsection
