@if(isset($produto->id))
    <form method="post" action="{{ route('produto.update', ['produto' => $produto->id]) }}">
        @csrf
        @method('PUT')
    @else
        <form method="post" action="{{ route('produto.store') }}">
            @csrf
@endif
<input type="text" name="nome" value="{{ $produto->nome ?? old('nome') }}" placeholder="Digite o nome do produto">
{{ $errors->has('nome') ? $errors->first('nome') : '' }}
<input type="text" name="descricao" placeholder="Digite a descricao do produto"
    value="{{ $produto->descricao ?? old('descricao') }}">
{{ $errors->has('descricao') ? $errors->first('descricao') : '' }}

<input type="text" name="peso" placeholder="Digite o peso do produto" value="{{ $produto->peso ?? old('peso') }}">
{{ $errors->has('peso') ? $errors->first('peso') : '' }}

<select name="unidade_id">
    <option value="">Selecione a unidade de medida</option>
    @foreach ($unidades as $unidade)
        <option value="{{ $unidade->id }}"
            {{ ($produto->unidade_id ?? old('unidade_id')) == $unidade->id ? 'selected' : '' }}>
            {{ $unidade->descricao }}</option>
        {{ $errors->has('unidade_id') ? $errors->first('unidade_id') : '' }}
    @endforeach
</select>

<button type="submit" class="borda-preta">Cadastrar</button>
</form>
