@extends('app.layouts.basico')
@section('titulo', 'Editar Produto')

@section('conteudo')
    <div class="conteudo-pagina">
        <div class="titulo-pagina-2">
            <p>Editar Produto</p>
        </div>
        <div class="menu">
            <li><a href="{{ route('produto.index') }}">Voltar</a></li>
        </div>
        <div class="informacao-pagina">
            <div style="width: 30%; margin-left: auto; margin-right: auto;">

                <form method="post" action="{{ route('produto.update', ['produto' => $produto->id]) }}">
                    @csrf
                    @method('PUT')
                    <select name="fornecedor_id">
                        <option value="">Selecione um fornecedor</option>
                        @foreach ($fornecedores as $fornecedor)
                            <option value="{{ $fornecedor->id }}"
                                {{ ($produto->fornecedor_id ?? old('fornecedor_id')) == $fornecedor->id ? 'selected' : '' }}>
                                {{ $fornecedor->nome }}</option>
                        @endforeach
                    </select>
                    {{ $errors->has('fornecedor_id') ? $errors->first('fornecedor_id') : '' }}

                    <input type="text" name="nome" value="{{ $produto->nome ?? old('nome') }}"
                        placeholder="Digite o nome do produto">
                    {{ $errors->has('nome') ? $errors->first('nome') : '' }}
                    <input type="text" name="descricao" placeholder="Digite a descricao do produto"
                        value="{{ $produto->descricao ?? old('descricao') }}">
                    {{ $errors->has('descricao') ? $errors->first('descricao') : '' }}

                    <input type="text" name="peso" placeholder="Digite o peso do produto"
                        value="{{ $produto->peso ?? old('peso') }}">
                    {{ $errors->has('peso') ? $errors->first('peso') : '' }}
                    <select name="unidade_id">
                        <option value="">Selecione a unidade de medida</option>
                        @foreach ($unidades as $unidade)
                            <option value="{{ $unidade->id }}"
                                {{ ($produto->unidade_id ?? old('unidade_id')) == $unidade->id ? 'selected' : '' }}>
                                {{ $unidade->descricao }}</option>
                            {{ $errors->has('unidade_id') ? $errors->first('unidade_id') : '' }}
                        @endforeach
                    </select>
                    <button type="submit" class="borda-preta">Editar</button>
                </form>
            </div>
        </div>
    </div>
@endsection
