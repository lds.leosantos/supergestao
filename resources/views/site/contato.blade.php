@extends('site.layouts.basico')
@section('titulo', 'Contato')
@section('conteudo')
    <div class="conteudo-pagina">
        <div class="titulo-pagina">
            <h1>Entre em contato conosco</h1>
        </div>
        <div class="informacao-pagina">
            <div class="contato-principal">
               @component('site.layouts._components.form_contato',['motivo_contatos'=>$motivo_contatos]);
               <p>A nossa equipe analisara a sua mensagem e retornaremos em breve!</p>Nosso tempo de resposta é de 24h</p>
               @endcomponent
            </div>
        </div>
    </div>
    @include('site.layouts._partials.rodape')
@endsection
