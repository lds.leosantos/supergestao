<?php

namespace App\Http\Middleware;

use Closure;


class AutenticacaoMiddeware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, string $metodo_autenticacao, $perfil)
    {

        return $next($request);
    }
}
