<?php

namespace App\Http\Controllers;

use App\Produto;
use App\Unidade;
use App\ProdutoDetalhe;
use App\Item;
use App\Fornecedor;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{

    public function index(Request $request)
    {
        $produtos = Item::with(['itemDetalhe','fornecedor'])->paginate(10);

        return view('app.produto.index', [
            'produtos' => $produtos,
            'request' => $request->all()

        ]);


    }


    public function create()
    {
        $unidades = Unidade::all();
        $fornecedores = Fornecedor::all();

        return view(
            'app.produto.create',
            [
                'unidades' => $unidades,
                'fornecedores' => $fornecedores
            ]
        );
    }


    public function store(Request $request)
    {


        $regras = [
            'nome' => 'required|min:3|max:50',
            'descricao' => 'required|min:3|max:2000',
            'peso' => 'required|integer',
            'unidade_id' => 'exists:unidades,id',
            'fornecedor_id' =>'exists:fornecedores,id'

        ];

        $feedback = [
            'required' => ' O campo :attribute deve ser prenchido',
            'nome.min' => 'O Campo nome deve ter no minino 3 caracteres',
            'nome.max' => 'O Campo nome deve ter no maximo 50 caracteres',
            'descricao.min' => 'O Campo descricao deve ter no minino 3 caracteres',
            'descricao.max' => 'O Campo descricao deve ter no maximo 2000 caracteres',
            'peso.integer' => 'o Campo peso deve ser um numnero inteiro',
            'unidade_id.exists' => ' A unidade de medida nao  existe',
            'fornecedor_id.exists' => ' O fornecedor informado  nao  existe',

        ];

        $request->validate($regras, $feedback);

         Item::create($request->all());

        return redirect()->route('produto.index');
    }


    public function show(Produto $produto)
    {

        return view('app.produto.show',[
            'produto' =>$produto
        ]);
    }


    public function edit(Produto $produto)
    {

        $unidades = Unidade::all();
        $fornecedores = Fornecedor::all();


        return view('app.produto.edit', [
           'produto' => $produto,
           'unidades' => $unidades,
           'fornecedores' => $fornecedores

        ]);

       // return view('app.produto.create', ['produto' => $produto,'unidades' => $unidades]);
    }


    public function update(Request $request, Item $produto)
    {

        $regras = [
            'nome' => 'required|min:3|max:50',
            'descricao' => 'required|min:3|max:2000',
            'peso' => 'required|integer',
            'unidade_id' => 'exists:unidades,id',
            'fornecedor_id' =>'exists:fornecedores,id'
        ];

        $feedback = [
            'required' => ' O campo :attribute deve ser prenchido',
            'nome.min' => 'O Campo nome deve ter no minino 3 caracteres',
            'nome.max' => 'O Campo nome deve ter no maximo 50 caracteres',
            'descricao.min' => 'O Campo descricao deve ter no minino 3 caracteres',
            'descricao.max' => 'O Campo descricao deve ter no maximo 2000 caracteres',
            'peso.integer' => 'o Campo peso deve ser um numnero inteiro',
            'unidade_id.exists' => ' A unidade de medida nao  existe',
            'fornecedor_id.exists' => ' O fornecedor informado  nao  existe',

        ];
        $produto->update($request->all());

        return redirect()->route('produto.show', ['produto'=> $produto->id]);

    }


    public function destroy(Produto $produto)
    {
        $produto->delete();
        return redirect()->route('produto.index');

    }
}
