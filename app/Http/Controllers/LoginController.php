<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        $erro = '';

        if ($request->get('erro') == 1) {
            $erro = 'Usuario ou senha nao  existe!';
        };

        if ($request->get('erro') == 2) {
            $erro = 'Necessário realizar login para ter acesso a pagina!';
        };
        return view(
            'site.login',
            [
                'titulo' => 'login',
                'erro' => $erro
            ]
        );
    }

    public function Autenticar(Request $request)
    {

        //regras de validaçao
        $regras = [
            'usuario' => 'email',
            'senha' => 'required',
        ];
        //as mensagens de feedabaki de validaçao
        $feedback = [
            'usuario.email' => 'O campo usuario (email) é obrigatorio',
            'senha.required' => 'O campo senha precisa ser prenchido',
        ];

        $request->validate($regras, $feedback);

        $email =  $request->get('usuario');
        $password = $request->get('senha');



        //inciar o ModelP
        $user = new User();
        $usuario = $user->where('email', $email)->where('password', $password)->get()->first();






        if ($usuario == true) {


            return redirect()->route('app.home', [
                'usuario' => $usuario,
            ]);
        } else {
            return redirect()->route('site.login', [
                'erro' => 1,
            ]);
        }
    }


    public function sair()
    {

            return redirect()->route('site.index/');
    }
}
