<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SiteContato;
use App\MotivoContato;

class ContatoController extends Controller
{
  public function contato(Request $request)
  {

    $motivo_contatos = MotivoContato::all();

    return view('site.contato', [
      'motivo_contatos' => $motivo_contatos,
    ]);
  }

  public function salvar(Request $request)
  {

    $request->validate(
      [
        'nome' => 'required|min:3|max:50|unique:site_contatos',
        'telefone' => 'required',
        'email' => 'email',
        'motivo_contatos_id' => 'required',
        'mensagem' => 'required|max:2000',
      ],
      [ 
          'nome.required'=>'O campo nome precisa ser prenchido',
          'nome.min'=>'O campo nome precisa ter no minimo 3 caracteres',
          'nome.max'=>'O campo nome precisa ter no maximo 50 caracteres',
          'nome.unique'=>'O nome ja esta em uso',
          'telefone.required'=>'O campo telefone precisa ser prenchido',
          'required'=>' O campo :attribute deve ser prenchido'
      ]
    );
    SiteContato::create($request->all());

    return redirect()->route('site.index');
  }
}
