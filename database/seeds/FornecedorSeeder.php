<?php

use Illuminate\Database\Seeder;
use App\Fornecedor;


class FornecedorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //instaniciando o objeto
        $fornecedor = new Fornecedor();
        $fornecedor->nome = 'Fornecedor 100';
        $fornecedor->site = 'fornecedor100.com.br';
        $fornecedor->uf = 'MG';
        $fornecedor->email = 'contato@fornecedor100.com.br';
        $fornecedor->save();


        //o metodo create (atenção para o atributo  fillable)
        Fornecedor::create([
            'nome' =>'Fornecedor 200',
            'site' =>'Fornecedor200.com.br',
            'uf' =>'SP',
            'email'=> 'contato@fornecedor200.com.br'
        ]);

        //insert

        DB::table('fornecedores')->insert([
            'nome' =>'Fornecedor 300',
            'site' =>'Fornecedor300.com.br',
            'uf' =>'CE',
            'email'=> 'contato@fornecedor300.com.br'
        ]);
    }
}
