<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProdutosRelacionamentosFornecedores extends Migration
{

    public function up()
    {
        //criando a coluna em produtos que vai receber a fk de fornecedores

        Schema::table('produtos', function (Blueprint $table) {

            //insere um registro de fornecedores para estabelcer o relacionado

            $fornecedor_id =   DB::table('fornecedores')->insertGetId([
                'nome' => 'Fornecedor Padrão 5G',
                'site' => 'wwww.forncedor.com.br',
                'uf' => 'CE',
                'email' => 'forncedorpadrao@gmail.com'
            ]);
            $table->unsignedBigInteger('fornecedor_id')->default($fornecedor_id)->after('id');
            $table->foreign('fornecedor_id')->references('id')->on('fornecedores');
        });
    }


    public function down()
    {
        Schema::table('produtos', function (Blueprint $table) {
            $table->dropForeign('produtos_fornecedor_id_foreign');
            $table->dropColumn('fornecedor_id');
        });
    }
}
